//
//  Ghost_HTests.m
//  Ghost-HTests
//
//  Created by Kurt Salman on 18.01.14.
//  Copyright (c) 2014 codesalman. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Ghost_HTests : XCTestCase

@end

@implementation Ghost_HTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
