//
//  ViewController.m
//  Ghost-H
//
//  Created by Kurt Salman on 18.01.14.
//  Copyright (c) 2014 codesalman. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_iOS7) {
        _imageViewRoterHead.frame = CGRectMake(0.0f, 0.0f, (_imageViewRoterHead.frame.size.width), _imageViewRoterHead.frame.size.height);
        
    }else{
        _imageViewRoterHead.frame = CGRectMake(0.0f, -20.0f, _imageViewRoterHead.frame.size.width, _imageViewRoterHead.frame.size.height);
    }
    
    _zBar = [[ZBarReaderViewController alloc] init];
    _zBar.readerDelegate = self;
    [_zBar.scanner setSymbology:ZBAR_CODABAR config:ZBAR_CFG_ENABLE to:0];
    _zBar.readerView.zoom = 1.0;
    
//    set individual view overlay
//    _zBar.cameraOverlayView = myOverlayView;
    

}

- (void)viewDidAppear:(BOOL)animated{

    
    if (_imageViewRoterHead.frame.origin.y == 0.0f || _imageViewRoterHead.frame.origin.y == -20.0f) {
        
        [UIView animateWithDuration:1.0f animations:^{
            
            if (IS_iOS7) {
                _imageViewRoterHead.frame = CGRectMake(0.0f, -(_imageViewRoterHead.frame.size.height), _imageViewRoterHead.frame.size.width, _imageViewRoterHead.frame.size.height);
                
            }else{
                _imageViewRoterHead.frame = CGRectMake(0.0f, -(_imageViewRoterHead.frame.size.height)-20.0f, _imageViewRoterHead.frame.size.width, _imageViewRoterHead.frame.size.height);
            }
        } completion:^(BOOL finished) {
            
            if (IS_IPHONE_5) {
                [UIView transitionWithView:_imageViewBlankBottom
                                  duration:0.6f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    _imageViewBlankBottom.image = [UIImage imageNamed:@"ghost_logo_bottom"];
                                } completion:NULL];
            }
        }];
    }
    
}



- (IBAction)push2Login:(id)sender {
    
    [_txtFieldPass resignFirstResponder];
    [_txtFieldHNR resignFirstResponder];
    
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];

    [MMProgressHUD showWithTitle:@"Ghost" status:@"Login..." images:@[[UIImage imageNamed:@"cycle1"],
               [UIImage imageNamed:@"cycle2"],
               [UIImage imageNamed:@"cycle3"]]];
    
    [MMProgressHUD dismissAfterDelay:3.0f];
    
    
}
- (IBAction)pushScan:(id)sender {
    
    
    [self presentViewController:_zBar animated:YES completion:^{}];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark zBar Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
//    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
//   [_zBar dismissViewControllerAnimated:YES completion:^{}];
    
    for(ZBarSymbol *sym in results) {
        NSLog(@"%@", sym.data);
        break;
    }
    
}


@end
