//
//  AppDelegate.h
//  Ghost-H
//
//  Created by Kurt Salman on 18.01.14.
//  Copyright (c) 2014 codesalman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
