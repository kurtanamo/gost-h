//
//  ViewController.h
//  Ghost-H
//
//  Created by Kurt Salman on 18.01.14.
//  Copyright (c) 2014 codesalman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMProgressHUD.h>
#import <ZBarSDK.h>

@interface ViewController : UIViewController <UITextFieldDelegate, ZBarReaderDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textFieldHNR;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (strong, nonatomic) IBOutlet UILabel *decodedLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewRoterHead;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBlankBottom;
@property (strong, nonatomic) IBOutlet UIView *viewLoginBox;
@property (strong, nonatomic) IBOutlet UIView *viewLogoutBox;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewScanButtonEnabled;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldHNR;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPass;

@property (strong, nonatomic) ZBarReaderViewController *zBar;

@property (strong, nonatomic) MMProgressHUD *myHUD;

@end
